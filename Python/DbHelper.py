#coding=utf-8 
# sqlserver的连接
import pymssql

class MSSQL1:
    def __init__(self,host,user,pwd,db):
        self.host = host
        self.user = user
        self.pwd = pwd
        self.db = db

    def __GetConnect(self):
        """
        得到连接信息
        返回: conn.cursor()
        """
        if not self.db:
            raise(NameError,"没有设置数据库信息")
        self.conn = pymssql.connect(host=self.host,user=self.user,password=self.pwd,database=self.db,charset="utf8")
        cur = self.conn.cursor()
        if not cur:
            raise(NameError,"连接数据库失败")
        else:
            return cur

    def ExecQuery(self,sql):
        """
        执行查询语句
        返回的是一个包含tuple的list，list的元素是记录行，tuple的元素是每行记录的字段

        """
        cur = self.__GetConnect()
        cur.execute(sql)
        resList = cur.fetchall()

        #查询完毕后必须关闭连接
        self.conn.close()
        return resList

    def ExecNonQuery(self,sql):
        """
        执行非查询语句

        调用示例：
            cur = self.__GetConnect()
            cur.execute(sql)
            self.conn.commit()
            self.conn.close()
        """
        cur = self.__GetConnect()
        ret = cur.execute(sql)
        self.conn.commit()
        self.conn.close()
        return ret
    
    def RunSQL(self,sql):
        return self.ExecNonQuery(sql)
    
    def RunSQLReturnDataTable(self,sql):
        return self.ExecQuery(sql)


def main():
    server="140.143.236.168:1433" #不指定端口时：server="127.0.0.1"
    user="ccbpmdemo"
    password="123"
    database="DevelopERPDemo"
    charset="utf8" #可省略
    ms = MSSQL1(host=server,user=user,pwd=password,db=database)
    resList = ms.RunSQLReturnDataTable("SELECT * FROM ERP_User")
    print(resList)

if __name__ == '__main__':
    main()