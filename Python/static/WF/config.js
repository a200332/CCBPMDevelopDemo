﻿
//驰骋BPM流程服务器地址.
var ccbpmHostDevelopAPI = "http://140.143.236.168:8078/DataUser/DevelopAPI.ashx";

/**
 * 执行URL转化为json对象.转化失败为null.
 * @param {url} url
 */
function RunUrlReturnJSON(url) {

    var str = RunUrlReturnString(url);
    if (str == null)  return null;

    try {
        return JSON.parse(str);
    } catch (e) {
        alert("json解析错误: " + str);
        return null;
    }
}

/**
 * 执行URL返回string.
 * @param {any} url
 */
function RunUrlReturnString(url) {

    if (url == null || typeof url === "undefined") {
        alert("err@url无效");
        return;
    }

    var string;

    $.ajax({
        type: 'post',
        async: false,
        url: url,
        dataType: 'html',
        xhrFields: {
            withCredentials: IsIELower10 == true ? false : true
        },
        crossDomain: IsIELower10 == true ? false : true,
        success: function (data) {
            if (data.indexOf("err@") != -1) {
                alert(data);
                return;
            }
            string = data;
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(url);
            alert("HttpHandler-RunUrlCrossReturnString-", textStatus);
        }
    });
    return string;
}


var IsIELower10 = false;

var ver = IEVersion();
if (ver == 6 || ver == 7 || ver == 8 || ver == 9)
    IsIELower10 = true;

function IEVersion() {
    var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串  
    var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1; //判断是否IE<11浏览器  
    var isEdge = userAgent.indexOf("Edge") > -1 && !isIE; //判断是否IE的Edge浏览器  
    var isIE11 = userAgent.indexOf('Trident') > -1 && userAgent.indexOf("rv:11.0") > -1;
    if (isIE) {
        if (document.documentMode) return document.documentMode;
    } else if (isEdge) {
        return 'edge';//edge
    } else if (isIE11) {
        return 11; //IE11  
    } else {
        return -1;//不是ie浏览器
    }
}

