#coding=utf-8 

# 导入Flask类
from flask import Flask

from flask import render_template
from flask import request
from flask import make_response
from flask import session
from flask import url_for
from flask import redirect

import pymssql
import json
import uuid

from DbHelper import MSSQL1

app = Flask(__name__)

@app.route('/login', methods = ['GET', 'POST'])
def login():
    return render_template('Portal/Login.htm')

@app.route('/Handler.ashx', methods = ['GET'])
def process():
    ret = ''
    if request.method == 'GET':
        try:
            doType = request.args['DoType']
        except:
            doType = request.args['DoWhat']
        
        if doType == "Portal_Login_Submit":
            ret = Portal_Login_Submit()
        elif doType == "AppDemo_CustomerList_Init":
            sql = "SELECT * FROM CRM_Customer "
            ret = RunSQLReturnDataTable(sql)
            ret = json.dumps(ret,ensure_ascii=False)
        return ret


def Portal_Login_Submit():
    no = request.args["No"]
    pwd = request.args["Pass"]
    # dt = RunSQLReturnDataTable("SELECT * FROM ERP_User WHERE BH='" + no + "'")
    sid = str(uuid.uuid4())
    sql = "UPDATE ERP_User SET SID='" + sid + "' WHERE BH='" + no + "'"
    RunSQL(sql)
    return sid

def RunSQLReturnDataTable(sql):
    conn = pymssql.connect(host='140.143.236.168:1433',user='ccbpmdemo',password="123",database='DevelopERPDemo',charset="utf8")
    cur = conn.cursor()
    cur.execute(sql)
    resList = cur.fetchall()
    conn.close()
    return resList

def RunSQL(sql):
    conn = pymssql.connect(host='140.143.236.168:1433',user='ccbpmdemo',password="123",database='DevelopERPDemo',charset="utf8")
    cur = conn.cursor()
    ret = cur.execute(sql)
    conn.commit()
    conn.close()
    return ret


if __name__ == '__main__':
    app.config['JSON_AS_ASCII'] = False
    app.run(host="0.0.0.0", port=8080, debug=True)