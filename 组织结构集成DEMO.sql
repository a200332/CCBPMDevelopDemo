/*
 组织结构集成Demo . 
 请参考操作手册 http://ccflow.org/docs 部分.
 一句话: 就是把ccbpm的组织结构表删除，创建一样结构的视图吧数据映射过来.
*/
USE DevelopCCBPMServicesDemo;

-- 集成用户.
DROP TABLE Port_Emp
Go
CREATE VIEW  Port_Emp 
AS
  SELECT BH as No, MC as Name,DeptID as FK_Dept,Password as Pass,SID,Tel,Email,PinYin,OrgNo,
0 as SignType, 0 as Idx FROM DevelopERPDemo.dbo.ERP_User
 

-- 集成部门.
DROP TABLE Port_Dept
Go
CREATE VIEW  Port_Dept 
AS
  SELECT BH as No, MC as Name, QuanCheng as NameOfPath, ParentBH as ParentNo, 
  0 as Idx, '' as OrgNo, '' as Leader
  FROM DevelopERPDemo.dbo.ERP_Dept  
  
-- 集成人员对应部门.
DROP TABLE Port_DeptEmp
Go
CREATE VIEW  Port_DeptEmp 
AS
  SELECT DeptID+'_'+UserID  as MyPK, DeptID as FK_Dept, UserID as FK_Emp
  FROM DevelopERPDemo.dbo.ERP_DeptUser
  
-- 集成岗位.
DROP TABLE Port_Station
Go
CREATE VIEW  Port_Station 
AS
  SELECT BH as No, MC as Name, RoleType as FK_StationType,
  0 as Idx, '' as OrgNo, '' as Leader
  FROM DevelopERPDemo.dbo.ERP_Role
  

-- 集成部门岗位人员.
DROP TABLE Port_DeptEmpStation
Go
CREATE VIEW  Port_DeptEmpStation 
AS
  SELECT  DeptID+'_'+UserID+'_'+RoleID as MyPK, DeptID as FK_Dept, UserID as FK_Emp, 
  RoleID as FK_Station, '' as OrgNo
FROM DevelopERPDemo.dbo.ERP_DeptUserRole
 
  
  


