package Controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;



public class BaseController {

	/**
	 * SpringMvc下获取request
	 * 
	 * @return
	 */
	public static HttpServletRequest getRequest() {
		try {
			RequestAttributes ra = RequestContextHolder.currentRequestAttributes();
			
			HttpServletRequest request = ((ServletRequestAttributes) ra).getRequest();
			//如果是文件上传的Request，需要强制转换为DefaultMultipartHttpServletRequest
			if(request.getAttribute("multipartRequest") != null){
				return (DefaultMultipartHttpServletRequest)(request.getAttribute("multipartRequest"));
			}else{
				return ((ServletRequestAttributes) ra).getRequest();
			}
		} catch (Exception e) {
			return null;
		}
	}

	public static HttpServletResponse getResponse() throws Exception {
		try {
			RequestAttributes ra = RequestContextHolder.currentRequestAttributes();
			
			return (HttpServletResponse)ra.getClass().getMethod("getResponse").invoke(ra);
			
		} catch (Exception e) {
			throw new Exception("获取HttpServletResponse失败");
		}
	}
	public String getParamter(String key){
		return getRequest().getParameter(key);
	}
	
	
	
}
