## 驰骋BPM开发Demo

#### 介绍

1. 该文件由驰骋公司提供的对于ccbpm独立服务模式调用接口的开发Demo.
2. 流程服务器单独部署, 其他应用采用调用的模式访问ccbpm提供的流程服务,从而完成应用.
3. 该目录提供了java, asp.net 两个版本的模式开发demo.
4. 提供了一个按照组件模式开发的表单，一个按照原始模式开发的表单应用场景.
5. 提供了一个crm系统如何使用代码启动ccbpm流程.
6. 该目录下的文件，可以改动，以完成您的需要. 也可以把WF组件目录copy到您的工程里进行开发.

#### 组件目录架构

1. /WF/Img 文件存放的工具栏上的按钮图片.
2. /WF/Scripts 存放的引用第三方的脚本.
3. /WF/Style 存放的风格文件.
4. ListAPI.js 是一些列表接口,比如:发起列表、待办列表、在途列表、已完成列表，用于生成相关的菜单列表.
5. Toolbar.js 工作处理器/工作查看器/抄送处理器的工具栏.
6. WorkCheck.js 审核组件.
7. Dev2Interface.js 驰骋BPM流程引擎的接口.
8. config.js  配置文件.

#### 安装教程 java
 
 暂未提供,请参考asp.net 版本教程.


#### 安装教程 asp.net

1.  使用gitee下载客户端开发demo代码.
2.  打开 D:\CCBPMDevelopDemo\ASPNET\CCBPMProjectDemo.sln 直接就可以运行,就可以打开驰骋BPM提供了官方网站的数据库连接.
3.  如果要连接到自己的驰骋BPM服务器，请打开 /WF/config.js的配置文件.
4.  修改本机的数据库连接 web.config .
 
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 其他资源

1.  下载驰骋BPM的各个版本 [http://ccflow.org/down.htm](http://ccflow.org/down.htm)
2.  视频教程 [http://ccflow.org/ke.htm](http://ccflow.org/ke.htm)
3.  官方网站 [http://ccflow.org](http://ccflow.org) 
4.  操作手册 [http://ccflow.org/docs](http://ccflow.org/docs) 


 