﻿//访问的ccbpm服务器地址.
var host = "http://140.143.236.168:8078";
//驰骋BPM流程服务器地址.
var ccbpmHostDevelopAPI = host + "/DataUser/DevelopAPI.ashx";

// 流程域名 , 默认为空. 比如:CRM, ERP, OA 等等。 该域名配置流程表单树的属性上.
// 表示该目录下所有的流程都属于这个域里.
var domain = "";

// for java
//var host = "http://172.16.0.69:8088/jflow-web";
// for java
// var ccbpmHostDevelopAPI = host + "/DataUser/DevelopAPI/ProcessRequest";


/**
 * 执行URL转化为json对象.转化失败为null.
 * @param {url} url
 */
function RunUrlReturnJSON(url) {

    var str = RunUrlReturnString(url);
    if (str == null) return null;

    try {
        return JSON.parse(str);
    } catch (e) {
        alert("json解析错误: " + str);
        return null;
    }
}

/**
 * 执行URL返回string.
 * @param {any} url
 */
function RunUrlReturnString(url) {

    if (url == null || typeof url === "undefined") {
        alert("err@url无效");
        return;
    }

    var string;

    $.ajax({
        type: 'post',
        async: false,
        url: url,
        dataType: 'html',
        xhrFields: {
            withCredentials: IsIELower10 == true ? false : true
        },
        crossDomain: IsIELower10 == true ? false : true,
        success: function (data) {
            if (data.indexOf("err@") != -1) {
                alert(data);
                return;
            }
            string = data;
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(url);
            alert("HttpHandler-RunUrlCrossReturnString-", textStatus);
        }
    });
    return string;
}

function basePath() {
    //获取当前网址，如： http://localhost:80/jflow-web/index.jsp  
    var curPath = window.document.location.href;
    //获取主机地址之后的目录，如： jflow-web/index.jsp
    var pathName = window.document.location.pathname;
    if (pathName == "/") { //说明不存在项目名
        if ("undefined" != typeof ccbpmPath && ccbpmPath != null && ccbpmPath != "") {
            if (ccbpmPath != curPath)
                return ccbpmPath;
        }
        return curPath;
    }
    var pos = curPath.indexOf(pathName);
    //获取主机地址，如： http://localhost:80  
    var localhostPath = curPath.substring(0, pos);
    //获取带"/"的项目名，如：/jflow-web
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);

    if ("undefined" != typeof ccbpmPath && ccbpmPath != null && ccbpmPath != "") {
        if (ccbpmPath != localhostPath)
            return ccbpmPath;
    }

    return localhostPath;
}


var IsIELower10 = false;

var ver = IEVersion();
if (ver == 6 || ver == 7 || ver == 8 || ver == 9)
    IsIELower10 = true;

function IEVersion() {
    var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串  
    var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1; //判断是否IE<11浏览器  
    var isEdge = userAgent.indexOf("Edge") > -1 && !isIE; //判断是否IE的Edge浏览器  
    var isIE11 = userAgent.indexOf('Trident') > -1 && userAgent.indexOf("rv:11.0") > -1;
    if (isIE) {
        if (document.documentMode) return document.documentMode;
    } else if (isEdge) {
        return 'edge';//edge
    } else if (isIE11) {
        return 11; //IE11  
    } else {
        return -1;//不是ie浏览器
    }
}


/**
* 动态异步加载JS的方法
* @param {any} url 加载js的路径
* @param {any} callback 加载完成后的回调函数
*/
function loadScript(url, callback, scriptID) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    if (callback != null && typeof (callback) != "undefined") {
        if (script.readyState) {
            script.onreadystatechange = function () {
                if (script.readyState == "loaded" || script.readyState == "complete") {
                    script.onreadystatechange = null;
                    callback();
                }
            };
        } else {
            script.onload = function () {
                callback();
            };
        }
    }
    script.src = url;
    if (scriptID != null && scriptID != undefined)
        script.id = scriptID;
    // document.head.appendChild(script);
    var tmp = document.getElementsByTagName('script')[0];
    tmp.parentNode.insertBefore(script, tmp);
}


/**
 * 父页面监听子页面调用方法
 */
window.addEventListener('message', function (event) {
    var data = event.data;
    var info = event.data.info;
    if (true) {
        switch (data.action) {
            case 'returnWorkWindowClose':
                if (typeof returnWorkWindowClose != 'undefined' && returnWorkWindowClose instanceof Function)
                    returnWorkWindowClose(info);
                break;
            case 'WindowCloseReloadPage':
                if (typeof WindowCloseReloadPage != 'undefined' && WindowCloseReloadPage instanceof Function)
                    WindowCloseReloadPage(info);
                break;
            default:
                break;
        }
    }
}, false);



