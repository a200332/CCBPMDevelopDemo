﻿
//页面启动函数.
$(function () {

    var sid = $.cookie("SID");
    if (sid == null || sid == '') {
        alert('登录信息丢失，请重新登录.');
        return;
    }
   
    //引入CSS样式
    $('head').append('<link href="' + host + '/DataUser/Style/CSS/Default/ccbpm.css" rel="stylesheet" type="text/css" />');
    $('head').append('<link href="' + host + '/DataUser/Style/MyFlow.css" rel="stylesheet" type="text/css" />');

    var JSUrl = host + "/WF/ccbpm.js?SID=" + sid + "&type=MyGener&Version=" + Math.random();
    loadScript(JSUrl, null, "ccbpmJS");

    //引入BootStrap的JS
    var basePathStr = basePath();
    $('head').append('<link href="' + basePathStr + '/WF/Scripts/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />');
    $('head').append('<link rel="stylesheet" href="' + basePathStr + '/WF/Scripts/bootstrap/css/css.css" type="text/css" media="all" />');
    loadScript(basePathStr + "/WF/Scripts/bootstrap/js/bootstrap.min.js",null);
    loadScript(basePathStr + "/WF/Scripts/bootstrap/BootstrapUIDialog.js",null);
    return;

});



