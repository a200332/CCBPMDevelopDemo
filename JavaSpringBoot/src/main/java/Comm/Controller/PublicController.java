package Comm.Controller;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;


import net.sf.json.JSONArray;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import Comm.tools.DBManager;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;


/**
 * Title: LoginController Description: 系统登录 Company:
 * 
 * @author
 * @date
 */
@Controller
@RequestMapping("/system")
@ResponseBody
public class PublicController extends BaseController {
	DBManager db = new DBManager();
	ResultSet rs = null;

	@RequestMapping("/PublicController")
	@ResponseBody
	public  String PublicController() throws Exception {

		String sql = "";
		String json = "";
		// 获取执行标记.
		String doType = this.getRequest().getParameter("DoType");

		if (doType == null || doType.equals("")) {
			doType = this.getRequest().getParameter("DoWhat");
		}

		// 执行登录.
		if (doType.equals("Portal_Login_Submit") == true) {
			String val= Portal_Login_Submit();
			return val;
			
		}
		// 获得客户列表.
		if (doType.equals("AppDemo_CustomerList_Init") == true) {
			sql = "SELECT * FROM CRM_Customer ";
		}
		// 获得开票列表.
		if (doType.equals("AppDemo_KaiPiaoList_Init") == true) {
			sql = "SELECT * FROM DevelopCCBPMServicesDemo.dbo.ND3Rpt ";
		}
		// 催款列表.
		if (doType.equals("AppDemo_CuiKuanList_Init") == true) {
			sql = "SELECT * FROM DevelopCCBPMServicesDemo.dbo.ND4Rpt ";
		}
		db = new DBManager();
		rs = db.query(sql);
		json = resultSetToJson(rs);
		return json;
	}

	public String Portal_Login_Submit() {
		// 生成一个GUID.
		String sid = getRandomFileName();
		try {
			String userNo = this.getRequest().getParameter("No");
			if (userNo == null) {
				return "err@用户名或者密码错误.";
			}

			String pass = this.getRequest().getParameter("Pass");
			// if (pass == null) {
			// return "err@用户名或者密码错误.";
			// }

			ResultSet rs = db.query("SELECT * FROM ERP_User WHERE BH='" + userNo + "'");

			if (!rs.next()) {
				return "err@用户名或者密码错误.";
			}
			String pwd = (String) rs.getObject("Password");
			// if (!pass.equals(pwd)) {
			// return "err@用户名或者密码错误.";
			// }

			// 更新到人员表里，如果您是集成模式，就需要更新您自己的用户表.
			// 这一点非常重要，因为需要sid就是token，要访问ccbpm的服务需要用到它做身份认证.
			String sql = "UPDATE ERP_User SET SID='" + sid + "' WHERE BH='" + userNo + "'";
			db = new DBManager();
			db.update(sql);
		 
			return sid;

		} catch (SQLException e) {

			e.printStackTrace();
			return "err@" + e.getMessage();

		}
	}

	public String resultSetToJson(ResultSet rs) throws SQLException, JSONException {
		// json数组
		JSONArray array = new JSONArray();

		// 获取列数
		ResultSetMetaData metaData = rs.getMetaData();
		int columnCount = metaData.getColumnCount();

		// 遍历ResultSet中的每条数据
		while (rs.next()) {
			JSONObject jsonObj = new JSONObject();

			// 遍历每一列
			for (int i = 1; i <= columnCount; i++) {
				String columnName = metaData.getColumnLabel(i);
				String value = rs.getString(columnName);
				jsonObj.put(columnName, value);
			}
			array.add(jsonObj);
		}

		return array.toString();
	}

	/**
	 * 生成随机文件数：当前年月日时分秒+五位随机数
	 * 
	 * @return
	 */
	public static String getRandomFileName() {
		String str = new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
		int rannum = (int) (new Random().nextDouble() * (999 - 100 + 1)) + 100;// 获取5位随机数
		return str + rannum;//

	}
}
