package Comm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JFlowApplication {

    public static void main(String[] args) {
        SpringApplication.run(JFlowApplication.class, args);
    }

}
